EESchema Schematic File Version 4
LIBS:controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Rozširujúci modul k Raspberry Pi 2"
Date "2019-05-15"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1050 950  1850 1450
U 5C703941
F0 "CAN" 60
F1 "can.sch" 60
$EndSheet
$Sheet
S 3350 950  1850 1450
U 5C705895
F0 "OT" 55
F1 "ot.sch" 55
$EndSheet
$Sheet
S 5650 950  1800 1450
U 5C747E71
F0 "RF" 55
F1 "rf.sch" 55
$EndSheet
$Sheet
S 7900 950  1850 1400
U 5C7522E9
F0 "Rpi" 55
F1 "rpi.sch" 55
$EndSheet
$EndSCHEMATC

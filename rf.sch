EESchema Schematic File Version 4
LIBS:controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Rozširujúci modul k Raspberry Pi 2"
Date "2019-05-15"
Rev "0.1"
Comp ""
Comment1 "Bezdrôtové rozhranie"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:RFM69HW U5
U 1 1 5C748261
P 5200 3650
F 0 "U5" H 5200 4200 55  0000 C CNN
F 1 "RFM69HW" H 5200 4100 55  0000 C CNN
F 2 "misc:RFM69HW" H 5200 3650 55  0001 C CNN
F 3 "" H 5200 3650 55  0001 C CNN
F 4 "TME:RFM69HW-433S2" H 300 450 50  0001 C CNN "Shop"
	1    5200 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR025
U 1 1 5C7484E1
P 4550 3100
F 0 "#PWR025" H 4550 2950 50  0001 C CNN
F 1 "+3V3" H 4550 3240 50  0000 C CNN
F 2 "" H 4550 3100 50  0001 C CNN
F 3 "" H 4550 3100 50  0001 C CNN
	1    4550 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5C748AD8
P 5800 4150
F 0 "#PWR027" H 5800 3900 50  0001 C CNN
F 1 "GND" H 5800 4000 50  0000 C CNN
F 2 "" H 5800 4150 50  0001 C CNN
F 3 "" H 5800 4150 50  0001 C CNN
	1    5800 4150
	1    0    0    -1  
$EndComp
Text GLabel 5800 3700 2    55   Input ~ 0
SCK
Text GLabel 5800 3600 2    55   Input ~ 0
MISO
Text GLabel 5800 3500 2    55   Input ~ 0
MOSI
Text GLabel 6000 3400 2    55   Input ~ 0
SS_RF
Text GLabel 4350 2350 1    55   Input ~ 0
RF_RST
$Comp
L controller-rescue:Conn_01x06 J22
U 1 1 5C74C8F4
P 3850 3150
F 0 "J22" V 4050 3100 50  0000 C CNN
F 1 "Conn_01x06" V 3950 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3850 3150 50  0001 C CNN
F 3 "" H 3850 3150 50  0001 C CNN
	1    3850 3150
	0    1    -1   0   
$EndComp
Wire Wire Line
	4700 4000 4550 4000
Wire Wire Line
	4550 4000 4550 3100
Wire Wire Line
	5800 3800 5800 4000
Wire Wire Line
	5800 3800 5700 3800
Connection ~ 5800 4000
Wire Wire Line
	5700 3400 5850 3400
Wire Wire Line
	5800 3500 5700 3500
Wire Wire Line
	5700 3600 5800 3600
Wire Wire Line
	5800 3700 5700 3700
Wire Wire Line
	4350 2850 4350 3300
Wire Wire Line
	4700 3400 4050 3400
Wire Wire Line
	4050 3350 4050 3400
Wire Wire Line
	4700 3500 3950 3500
Wire Wire Line
	3950 3500 3950 3350
Wire Wire Line
	4700 3600 3850 3600
Wire Wire Line
	3850 3350 3850 3600
Wire Wire Line
	4700 3700 3750 3700
Wire Wire Line
	3750 3700 3750 3350
Wire Wire Line
	4700 3800 3650 3800
Wire Wire Line
	3650 3800 3650 3350
Wire Wire Line
	4700 3900 3550 3900
Wire Wire Line
	3550 3900 3550 3350
Text GLabel 4050 4100 3    55   Input ~ 0
RF_DIO0
Connection ~ 4050 3400
Text GLabel 3850 4100 3    55   Input ~ 0
RF_DIO2
Connection ~ 3850 3600
$Comp
L controller-rescue:Conn_01x01 J23
U 1 1 5C74CB9C
P 5350 4750
F 0 "J23" H 5350 4850 50  0000 C CNN
F 1 "Conn_01x01" H 5350 4650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5350 4750 50  0001 C CNN
F 3 "" H 5350 4750 50  0001 C CNN
	1    5350 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4750 5150 4750
$Comp
L controller-rescue:R R10
U 1 1 5C74CDED
P 7100 3900
F 0 "R10" V 7000 3900 50  0000 C CNN
F 1 "0" V 7200 3900 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 7030 3900 50  0001 C CNN
F 3 "" H 7100 3900 50  0001 C CNN
F 4 "GME:900-001" H 300 450 50  0001 C CNN "Shop"
	1    7100 3900
	0    1    1    0   
$EndComp
$Comp
L controller-rescue:Conn_01x02 J25
U 1 1 5C74CEB7
P 6450 3650
F 0 "J25" V 6650 3600 50  0000 C CNN
F 1 "Conn_01x02" V 6550 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6450 3650 50  0001 C CNN
F 3 "" H 6450 3650 50  0001 C CNN
	1    6450 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 4000 5700 4000
$Comp
L power:GND #PWR026
U 1 1 5C74D02D
P 5050 4950
F 0 "#PWR026" H 5050 4700 50  0001 C CNN
F 1 "GND" H 5050 4800 50  0000 C CNN
F 2 "" H 5050 4950 50  0001 C CNN
F 3 "" H 5050 4950 50  0001 C CNN
	1    5050 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4750 5050 4950
Wire Wire Line
	6550 3850 6550 3900
Connection ~ 6550 3900
$Comp
L power:GND #PWR028
U 1 1 5C74D1BD
P 6450 4050
F 0 "#PWR028" H 6450 3800 50  0001 C CNN
F 1 "GND" H 6450 3900 50  0000 C CNN
F 2 "" H 6450 4050 50  0001 C CNN
F 3 "" H 6450 4050 50  0001 C CNN
	1    6450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3900 6550 3900
$Comp
L controller-rescue:Conn_01x01 J26
U 1 1 5C74D2DC
P 7650 3900
F 0 "J26" H 7650 4000 50  0000 C CNN
F 1 "Conn_01x01" H 7650 3800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 7650 3900 50  0001 C CNN
F 3 "" H 7650 3900 50  0001 C CNN
	1    7650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3900 7450 3900
$Comp
L controller-rescue:Conn_01x01 J24
U 1 1 5C752247
P 6200 2900
F 0 "J24" H 6200 3000 50  0000 C CNN
F 1 "Conn_01x01" H 6200 2800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6200 2900 50  0001 C CNN
F 3 "" H 6200 2900 50  0001 C CNN
	1    6200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2900 5850 2900
Wire Wire Line
	5850 2900 5850 3400
Connection ~ 5850 3400
Wire Wire Line
	4350 3300 4700 3300
$Comp
L controller-rescue:R R15
U 1 1 5C762DA4
P 4350 2700
F 0 "R15" H 4200 2600 50  0000 C CNN
F 1 "inf" H 4200 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4280 2700 50  0001 C CNN
F 3 "" H 4350 2700 50  0001 C CNN
	1    4350 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 2350 4350 2550
Wire Wire Line
	6450 3850 6450 4050
Wire Wire Line
	5800 4000 5800 4150
Wire Wire Line
	4050 3400 4050 4100
Wire Wire Line
	3850 3600 3850 4100
Wire Wire Line
	6550 3900 6950 3900
Wire Wire Line
	5850 3400 6000 3400
$EndSCHEMATC
